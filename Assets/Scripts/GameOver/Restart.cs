﻿using System.Collections;
using System.Collections.Generic;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Restart : MonoBehaviour, IInterstitialAdListener {

    private void Start () {
       
        Appodeal.setInterstitialCallbacks (this);
    }
    public void GoRestart () {
        gameObject.GetComponent<Button> ().interactable = false;
        StartCoroutine (GetRequest ("https://www.google.com"));
    }
    IEnumerator GetRequest (string uri) {
        using (UnityWebRequest webRequest = UnityWebRequest.Get (uri)) {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest ();
            string[] pages = uri.Split ('/');
            int page = pages.Length - 1;
            if (webRequest.isNetworkError) {
                ReLoad ();
            } else {
                ShowInterstitial ();
            }
            gameObject.GetComponent<Button> ().interactable = true;
        }
    }
    public void ReLoad () {
        SceneManager.LoadScene (LevelManager.lm_Instance.choosenLevel);
        // SceneManager.LoadScene("GamePlay_Level1");
    }

    private void ShowInterstitial () {
        if (Appodeal.isLoaded (Appodeal.INTERSTITIAL)) //Если реклама загружена, то показывается
            Appodeal.show (Appodeal.INTERSTITIAL);
    }

    public void onInterstitialLoaded (bool isPrecache) {

    }

    public void onInterstitialFailedToLoad () {

    }

    public void onInterstitialShown () {
        throw new System.NotImplementedException ();
    }

    public void onInterstitialClosed () {
        ReLoad ();
    }

    public void onInterstitialClicked () {
        throw new System.NotImplementedException ();
    }

    public void onInterstitialExpired () {
        ReLoad ();
    }
}