﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AboutHub : MonoBehaviour {
    public GameObject SetVersRus;
    public GameObject SetVersEnu;
    GameObject ActiveVers;
    void Start () {
        if (Lean.Localization.LeanLocalization.CurrentLanguage == "Russian") {
            ActiveVers = Instantiate (SetVersRus, new Vector3 (-320, 14, -125), Quaternion.identity);
        } else {
            ActiveVers = Instantiate (SetVersEnu, new Vector3 (-320, 14, -125), Quaternion.identity);
        }
        ActiveVers.transform.SetParent (this.transform);
    }
}