﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsHub : MonoBehaviour {
    // Start is called before the first frame update
    public GameObject SetVersRus;
    public GameObject SetVersEnu;
    GameObject ActiveVers;

    void Start () {
        if (Lean.Localization.LeanLocalization.CurrentLanguage == "Russian") {
           ActiveVers =   Instantiate (SetVersRus, new Vector3 (-200, 14, -125), Quaternion.identity);      
        } else {
           ActiveVers =   Instantiate (SetVersEnu, new Vector3 (-200, 14, -125), Quaternion.identity);
        }
      ActiveVers.transform.SetParent(this.transform);
    }
}