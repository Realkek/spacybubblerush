﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {
    public static LevelManager lm_Instance { get; set; }
    public string choosenLevel;
   
    void Awake () {
        if (lm_Instance == null) {
            lm_Instance = this;
        } else if (lm_Instance != null) {
            Destroy (gameObject);
        }
    }

}