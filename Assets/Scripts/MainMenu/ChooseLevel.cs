﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseLevel : MonoBehaviour {
    [SerializeField]
    private byte lvl = 0;
    public void ChooseLvl () {
        switch (lvl) {
            case 1:
                LevelManager.lm_Instance.choosenLevel = "GamePlay_Level1";
                break;
            case 2:
                LevelManager.lm_Instance.choosenLevel = "GamePlay_Level2";
                break;
            case 3:
                LevelManager.lm_Instance.choosenLevel = "GamePlay_Level3";
                break;
            case 4:
                LevelManager.lm_Instance.choosenLevel = "GamePlay_Level4";
                break;
            case 5:
                LevelManager.lm_Instance.choosenLevel = "GamePlay_Level5";
                break;
            case 6:
                LevelManager.lm_Instance.choosenLevel = "GamePlay_Level6";
                break;
        }
        GoPlay.gp_Instance.StartGame ();
    }
}