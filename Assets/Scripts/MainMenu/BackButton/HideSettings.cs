﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HideSettings : MonoBehaviour {
    public static HideSettings q_Instance { get; set; }
    GameObject CameraPos;

    void Awake () {
        if (q_Instance == null) {
            q_Instance = this;
        } 
    }
    void Update () {
        // if (Input.GetKeyDown (KeyCode.Escape)) {
        //     BackFromPanel ();
        // }
    }
    public void BackFromPanel () {
        if (SceneManager.GetActiveScene ().name == "MainMenu") {
            GameObject StartCameraPos = GameObject.Find ("StartCamPos");
            transform.GetChild (0).gameObject.SetActive (false);
            CameraPos = GameObject.FindGameObjectWithTag ("MainCamera");
            if (CameraPos.transform.position != StartCameraPos.transform.position) {
                CameraPos.transform.position = StartCameraPos.transform.position;
            } else if (CameraPos.transform.position == StartCameraPos.transform.position) {
                Application.Quit ();
            }
        }
    }
}