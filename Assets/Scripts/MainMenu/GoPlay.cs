﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GoPlay : MonoBehaviour {
    public static GoPlay gp_Instance { get; set; }
    void Awake () {
        if (gp_Instance == null) {
            gp_Instance = this;
        } else if (gp_Instance != null) {
            Destroy (gameObject);
        }
    }
    private void Start () {
        SoundsOnOff ();
    }

    
    public void StartGame () {
        string choosenScene = LevelManager.lm_Instance.choosenLevel;
        int isAvalibleLvl;
        if (choosenScene != "GamePlay_Level1") {
            isAvalibleLvl = Load.LoadData (choosenScene, 0);
            if (isAvalibleLvl == 1) {
                SceneManager.LoadScene (choosenScene);
            }
        } else if (choosenScene == "GamePlay_Level1") {
            SceneManager.LoadScene (choosenScene);
        }
    }
    public void Showlvl () {
        LevelPanel.lp_Instance.ShowPanel ();
        GameObject NewCameraPos = GameObject.FindGameObjectWithTag ("MainCamera");
        NewCameraPos.transform.position = GameObject.Find ("LevelsCamPos").transform.position;
    }
    public void ActivateSToggle(){
           SoundsToggle.st_Instance.onChangedToggle += SoundsOnOff;
    }
    public void SoundsOnOff () {
        string sT = PlayerPrefs.GetString ("soundsToggle");
        switch (sT) {
            case "On":
                AudioListener.volume = 0.4f;
                break;
            case "Off":
                AudioListener.volume = 0;
                break;
        }
    }
    
}