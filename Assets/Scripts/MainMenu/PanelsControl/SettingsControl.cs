﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsControl : MonoBehaviour {
    // Start is called before the first frame update
    public static SettingsControl sc_Instance { get; set; }
    private GameObject settingsPanel;
    void Awake () {
        if (sc_Instance == null) {
            sc_Instance = this;
        } else if (sc_Instance != null) {
            Destroy (gameObject);
        }
    }
    void Start () {
        settingsPanel = transform.GetChild (0).gameObject;
    }
    public void ShowPanel () {
        settingsPanel.SetActive (true);
        GameObject NewCameraPos = GameObject.FindGameObjectWithTag ("MainCamera");
        NewCameraPos.transform.position = GameObject.Find ("NewSettingsCamPos").transform.position;
    }
}