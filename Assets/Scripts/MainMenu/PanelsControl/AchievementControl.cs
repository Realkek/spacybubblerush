﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementControl : MonoBehaviour {
    public static AchievementControl ac_Instance { get; set; }
    private GameObject AchievementPanel;
    void Awake () {
        if (ac_Instance == null) {
            ac_Instance = this;
        } else if (ac_Instance != null) {
            Destroy (gameObject);
        }
    }

    void Start () {
        AchievementPanel = GameObject.Find ("AchievementPanel");
    }
    public void ShowPanel () {
        AchievementPanel.SetActive (true);
        GameObject NewCameraPos = GameObject.FindGameObjectWithTag ("MainCamera");
        NewCameraPos.transform.position = GameObject.Find ("NewAchieveCamPos").transform.position;
        AchieveController.ac_Instance.GetAllScores ();
    }
}