﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AboutControl : MonoBehaviour
{
    public static AboutControl ac_Instance { get; set; }
    private GameObject AboutPanel;
    void Awake () {
        if (ac_Instance == null) {
            ac_Instance = this;
        } else if (ac_Instance != null) {
            Destroy (gameObject);
        }
    }

    void Start () {
        AboutPanel = transform.GetChild (0).gameObject;
    }

    public void ShowPanel () {
        AboutPanel.SetActive (true);
        GameObject NewCameraPos = GameObject.FindGameObjectWithTag ("MainCamera");
        NewCameraPos.transform.position = GameObject.Find ("NewAboutCamPos").transform.position;
    }
}
