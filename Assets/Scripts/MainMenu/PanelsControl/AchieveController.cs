﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchieveController : MonoBehaviour {
    string[] levels = { "GamePlay_Level1", "GamePlay_Level2", "GamePlay_Level3", "GamePlay_Level4", "GamePlay_Level5", "GamePlay_Level6" };
    GameObject[] levelsScoreTxt;
    public static AchieveController ac_Instance { get; set; }
    void Awake () {
        if (ac_Instance == null) {
            ac_Instance = this;
        } else if (ac_Instance != null) {
            Destroy (gameObject);
        }
    }
    private void Start () {
        levelsScoreTxt = new GameObject[levels.Length];
    }
    public void GetAllScores () {
        for (int i = 0; i < levels.Length; i++) {
            levelsScoreTxt[i] = transform.GetChild (i + 1).gameObject;
            string currentMinute = levels[i] + "minute";
            string currentSecond = levels[i] + "second";
            int m_Score = PlayerPrefs.GetInt (currentMinute);
            int s_Score = PlayerPrefs.GetInt (currentSecond);
            string newTxt = " " + m_Score + " : " + s_Score;
            if (levelsScoreTxt[i].GetComponent<Text> ().text.Contains (newTxt) != true)
                levelsScoreTxt[i].GetComponent<Text> ().text += newTxt;
        }
    }

    public void CheckScore (int minute, int second) {
        string currentLevel = LevelManager.lm_Instance.choosenLevel;
        string currentMinute = currentLevel + "minute";
        string currentSecond = currentLevel + "second";
        int m_Score = PlayerPrefs.GetInt (currentMinute);
        int s_Score = PlayerPrefs.GetInt (currentSecond);
        if (minute > 0 || second > 0) {
            if (minute < m_Score || (m_Score == 0 && second < s_Score)) {
                PlayerPrefs.SetInt (currentMinute, minute);
                PlayerPrefs.SetInt (currentSecond, second);
            } else if (m_Score == 0 && s_Score == 0) {
                PlayerPrefs.SetInt (currentMinute, minute);
                PlayerPrefs.SetInt (currentSecond, second);
            }
        }
    }
}