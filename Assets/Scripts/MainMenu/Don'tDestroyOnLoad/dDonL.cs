﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dDonL : MonoBehaviour {
  public static dDonL ddl_Instance { get; set; }

  void Awake () {
    if (ddl_Instance == null) {
      ddl_Instance = this;
    } else if (ddl_Instance != null) {
      Destroy (gameObject);
    }
    GameObject objs = GameObject.Find ("SettingsPanel");
    DontDestroyOnLoad (this.gameObject);

  }

}