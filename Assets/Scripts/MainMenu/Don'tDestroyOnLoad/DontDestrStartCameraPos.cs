﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestrStartCameraPos : MonoBehaviour
{
     public static DontDestrStartCameraPos ddscp_Instance { get; set; }
    void Awake () {
        if (ddscp_Instance == null) {
            ddscp_Instance = this;
        } else if (ddscp_Instance != null) {
            Destroy (gameObject);
        }
        DontDestroyOnLoad (this.gameObject);
    }
}
