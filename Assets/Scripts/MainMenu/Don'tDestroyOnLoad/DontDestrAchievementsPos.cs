﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestrAchievementsPos : MonoBehaviour
{
    public static DontDestrAchievementsPos ddap_Instance { get; set; }
    void Awake () {
        if (ddap_Instance == null) {
            ddap_Instance = this;
        } else if (ddap_Instance != null) {
            Destroy (gameObject);
        }
        DontDestroyOnLoad (this.gameObject);
    }
}
