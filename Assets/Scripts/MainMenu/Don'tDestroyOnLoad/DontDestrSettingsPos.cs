﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestrSettingsPos : MonoBehaviour {
    public static DontDestrSettingsPos ddsp_Instance { get; set; }
    void Awake () {
        if (ddsp_Instance == null) {
            ddsp_Instance = this;
        } else if (ddsp_Instance != null) {
            Destroy (gameObject);
        }
        DontDestroyOnLoad (this.gameObject);
    }
}