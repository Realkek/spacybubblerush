﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestrAboutPos : MonoBehaviour {

    public static DontDestrAboutPos ddap_Instance { get; set; }
    void Awake () {
        if (ddap_Instance == null) {
            ddap_Instance = this;
        } else if (ddap_Instance != null) {
            Destroy (gameObject);
        }
        DontDestroyOnLoad (this.gameObject);
    }
}