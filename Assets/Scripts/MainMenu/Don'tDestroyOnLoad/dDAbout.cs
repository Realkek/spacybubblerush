﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dDAbout : MonoBehaviour
{
     public static dDAbout dda_Instance { get; set; }
  void Awake () {
    if (dda_Instance == null) {
      dda_Instance = this;
    } else if (dda_Instance != null) {
      Destroy (gameObject);
    }
    DontDestroyOnLoad (this.gameObject);

  }
}
