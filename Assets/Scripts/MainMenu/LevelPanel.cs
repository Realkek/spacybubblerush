﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelPanel : MonoBehaviour {
    public static LevelPanel lp_Instance { get; set; }
    void Awake () {

        if (lp_Instance == null) {
            lp_Instance = this;
        } else if (lp_Instance != null) {
            Destroy (gameObject);
        }
    }


public void ShowPanel(){
    transform.GetChild(0).gameObject.SetActive(true);
}
   

   
}