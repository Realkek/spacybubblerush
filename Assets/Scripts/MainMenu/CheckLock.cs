﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckLock : MonoBehaviour {

    public static CheckLock cL_Instance { get; set; }
    string[] levels = new string[] { "GamePlay_Level2", "GamePlay_Level3", "GamePlay_Level4", "GamePlay_Level5", "GamePlay_Level6" };
    void Start () {
        foreach (string level in levels) {
            if (PlayerPrefs.HasKey (level)) {
                switch(level){
                    case "GamePlay_Level2": transform.GetChild(0).gameObject.SetActive(false);
                    break;
                    case "GamePlay_Level3": transform.GetChild(1).gameObject.SetActive(false);
                    break;
                    case "GamePlay_Level4": transform.GetChild(2).gameObject.SetActive(false);
                    break;
                    case "GamePlay_Level5": transform.GetChild(3).gameObject.SetActive(false);
                    break;
                    case "GamePlay_Level6": transform.GetChild(4).gameObject.SetActive(false);
                    break;  
                }
            }
        }
    }

    public void HideLock () {
        gameObject.SetActive (false);
    }
}