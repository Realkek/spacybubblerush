﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandDividend : MonoBehaviour {
    // Start is called before the first frame update
    public static RandDividend rd_Instance {
        get;
        set;
    }
    string levelNow;
    public int deviderCount; // количество делителей в ливеле
    public int[] comparer; // счетчик количества возможных делимых для каждого из делителей
    public List<int> forCompR = new List<int> (); //  comparer again, but for translate to clickHandler
    public int dividerNum = 0; // номер делителя в списке
    public int dividendCC = 0; // счетчик уже отмеченных из возможных делимых
    void Start () {
        deviderCount = DeviderCounting (LevelManager.lm_Instance.choosenLevel);
        comparer = new int[deviderCount];
    }

    void Awake () {
        if (rd_Instance == null) {
            rd_Instance = this;
        } else if (rd_Instance != null) {
            Destroy (gameObject);
        }
    }

    public int DeviderCounting (string levelNow) {
        int count = 0;
        switch (levelNow) {
            case "GamePlay_Level1":
                count = 3;
                break;
            case "GamePlay_Level2":
                count = 3;
                break;
            case "GamePlay_Level3":
                count = 3;
                break;
            case "GamePlay_Level4":
                count = 4;
                break;
            case "GamePlay_Level5":
                count = 4;
                break;
            case "GamePlay_Level6":
                count = 4;
                break;
        }
        return count;
    }
    public void ClearColors () {
        for (int i = 0; i < transform.childCount; i++) {
            Renderer rend;
            rend = transform.GetChild (i).GetComponent<Renderer> ();
            rend.material.color = Color.blue;
        }

    }
    public void RndDividend (int[] arrayList) { //рандомит кратные делителям делимые
        levelNow = LevelManager.lm_Instance.choosenLevel;
        int digit = 0; // делимое
        int choosenDivider; //текущий делитель
        TextMesh textObject; //число на шарик
        for (int i = 0; i < transform.childCount; i++) {
            textObject = transform.GetChild (i).GetChild (0).GetComponent<TextMesh> ();
            int devNum = RndDevNum (); //номер случайного из делителей
           
                do {
                    switch (levelNow) {
                        case "GamePlay_Level1":
                            digit = UnityEngine.Random.Range (10, 50);
                            break;
                        case "GamePlay_Level2":
                            digit = UnityEngine.Random.Range (10, 50);
                            break;
                        case "GamePlay_Level3":
                            digit = UnityEngine.Random.Range (20, 100);
                            break;
                        case "GamePlay_Level4":
                            digit = UnityEngine.Random.Range (20, 200);
                            break;
                        case "GamePlay_Level5":
                            digit = UnityEngine.Random.Range (20, 700);
                            break;
                        case "GamePlay_Level6":
                            digit = UnityEngine.Random.Range (20, 1000);
                            break;
                    }
                    choosenDivider = arrayList[devNum]; // выбирается случайный делитель из  доступных 
                }
                while (digit % choosenDivider != 0); // пока делимое не делится без остатка на один случайный из имеющихся делителей 
                textObject.text = digit.ToString ();
        }
         TrueCompare ();
    }

    public int RndDevNum () {
        int devNum = 0;
        switch (levelNow) {
            case "GamePlay_Level1":
                devNum = UnityEngine.Random.Range (0, 2);;
                break;
            case "GamePlay_Level2":
                devNum = UnityEngine.Random.Range (0, 2);;
                break;
            case "GamePlay_Level3":
                devNum = UnityEngine.Random.Range (0, 2);;
                break;
            case "GamePlay_Level4":
                devNum = UnityEngine.Random.Range (0, 3);;
                break;
            case "GamePlay_Level5":
                devNum = UnityEngine.Random.Range (0, 3);;
                break;
            case "GamePlay_Level6":
                devNum = UnityEngine.Random.Range (0, 3);;
                break;
        }
        return devNum;
    }

    public void TrueCompare () {
        comparer = DividendCount (RandDivider.rd_Instance.dividersText); //количество подходящих делимных для делителей
        forCompR.Clear ();
        foreach (int i in comparer) {
            forCompR.Add (i);
            if (i == 0) { //если нет подходящих делимых
                forCompR.Clear ();
                RandDivider.rd_Instance.RndDiv ();
            }
        }
    }

    public int[] DividendCount (int[] arrayList) { // считает количество возможных делимых для каждого из делителей
        TextMesh textObject = null;
        int[] countArr = new int[arrayList.Length];
        // try {
        for (int i = 0; i < arrayList.Length; i++) {
            int counter = 0;
            for (int j = 0; j < transform.childCount; j++) {
                textObject = transform.GetChild (j).GetChild (0).GetComponent<TextMesh> ();
                if (Convert.ToInt32 (textObject.text) % arrayList[i] == 0) {
                    counter++;
                }
                textObject = null;
            }
            countArr[i] = counter;
        }
        // } catch (Exception) { }
        return countArr;
    }

}