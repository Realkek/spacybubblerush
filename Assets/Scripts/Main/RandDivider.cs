﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandDivider : MonoBehaviour {

    string levelNow;
    public static RandDivider rd_Instance {
        get;
        set;
    }

    void Awake () {
        if (rd_Instance == null) {
            rd_Instance = this;
        } else if (rd_Instance != null) {
            Destroy (gameObject);
        }
    }

    public int[] dividersText; // набор делителей
    // Start is called before the first frame update

    void Start () {
        levelNow = LevelManager.lm_Instance.choosenLevel;
        RndDiv ();
        // while (RandDividend.rd_Instance.forCompR.Count == 0 || RandDividend.rd_Instance.forCompR.Contains (0))
        //     RandDividend.rd_Instance.RndDividend (dividersText);
    }

    public void RndDiv () { //рандомит делители
        TextMesh textObject;
        dividersText = null;
        dividersText = new int[RandDividend.rd_Instance.DeviderCounting (LevelManager.lm_Instance.choosenLevel)]; //количество делителей
        int rnd = 0; //конкретное случайное делимое
        for (int i = 0; i < transform.childCount; i++) {
            textObject = transform.GetChild (i).GetChild (0).GetComponent<TextMesh> (); //запись на шарике
            switch (levelNow) {
                case "GamePlay_Level1":
                    rnd = Random.Range (2, 10);
                    break;
                case "GamePlay_Level2":
                    rnd = Random.Range (2, 20);
                    break;
                case "GamePlay_Level3":
                    rnd = Random.Range (15, 36);
                    break;
                case "GamePlay_Level4":
                    rnd = Random.Range (15, 36);
                    break;
                case "GamePlay_Level5":
                    rnd = Random.Range (31, 50);
                    break;
                case "GamePlay_Level6":
                    rnd = Random.Range (31, 50);
                    break;
            }
            dividersText[i] = rnd;
            textObject.text = rnd.ToString ();
        }
        RandDividend.rd_Instance.RndDividend (dividersText);
        if (HasDuplicates (dividersText)) {
            RndDiv ();
            return;
        }
        
    }

    // public void Dividend () {
    //     RandDividend.rd_Instance.RndDividend (dividersText); // рандомятся кратные делимые
    // }

    private bool HasDuplicates (int[] arrayList) { //проверяет делители на повторы
        List<int> vals = new List<int> ();
        bool returnValue = false;
        foreach (int s in arrayList) {
            if (vals.Contains (s)) {
                returnValue = true;
                break;
            }
            vals.Add (s);
        }

        return returnValue;
    }
}