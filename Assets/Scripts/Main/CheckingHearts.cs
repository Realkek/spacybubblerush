﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckingHearts : MonoBehaviour {
    public static CheckingHearts ch_Instance { get; set; }
    public byte hpCount = 2;
    public void Awake () {
        ch_Instance = this;
    }
    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }

    public void hpMinus () {
        if (hpCount > 0) {
            transform.GetChild (hpCount).gameObject.SetActive (false);
            hpCount--;
            WrongChoice.wc_Instance.GoSound();
        } else {
            transform.GetChild (hpCount).gameObject.SetActive (false);
            SceneManager.LoadScene("GameOver");
        }
    }
}