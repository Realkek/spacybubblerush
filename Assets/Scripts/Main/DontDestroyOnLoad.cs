﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnLoad : MonoBehaviour {

    public static DontDestroyOnLoad ddOL_Instance { get; set; }
    void Awake () {

        if (ddOL_Instance == null) {
            ddOL_Instance = this;
        } else if (ddOL_Instance != null) {
            Destroy (gameObject);
        }

        GameObject[] objs = GameObject.FindGameObjectsWithTag ("music");

        if (objs.Length > 1) {
            Destroy (this.gameObject);
        }

        DontDestroyOnLoad (this.gameObject);
    }
}