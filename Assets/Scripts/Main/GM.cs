﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GM : MonoBehaviour {

    public static GM gm_Instance { get; set; }
    public bool winCheker = false;
    public bool gmStatusCheker = true;

    void Awake () {
        if (gm_Instance == null) {
            gm_Instance = this;
        } else if (gm_Instance != null) {
            Destroy (gameObject);
        }
    }
}