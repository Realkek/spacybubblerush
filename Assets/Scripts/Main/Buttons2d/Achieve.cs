﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Achieve : MonoBehaviour {
    public static Achieve t_Instance { get; set; }
    GameObject startCanvas;
    Transform setCanvas;
    GameObject settFromCanvas;
    GameObject MCamera;
    Vector3 StartPosMCamera;
    void Awake () {
        if (t_Instance == null) {
            t_Instance = this;
        } else if (t_Instance != null) {
            Destroy (gameObject);
        }
    }
    private void Start () {
        MCamera = GameObject.FindGameObjectWithTag ("MainCamera");
        StartPosMCamera = MCamera.transform.position;
    }
    private void Update () {
        if (Input.GetKeyDown (KeyCode.Escape)) {
            BackPanel ();
        }
    }
    public void OnMouseDown () {
        if (GM.gm_Instance.winCheker == false) {
            DisplayPanel ();
        }
    }

    public void DisplayPanel () {

        GameObject SCamera = GameObject.Find ("AchievementtCameraPos");
        MCamera.transform.position = SCamera.transform.position;
        settFromCanvas = GameObject.Find ("AchievementtCanvas");
        setCanvas = AchievementControl.ac_Instance.transform.parent;
        setCanvas.GetChild (0).GetChild (0).gameObject.SetActive (true);
        setCanvas.position = settFromCanvas.transform.position;
        setCanvas.rotation = settFromCanvas.transform.rotation;
        setCanvas.localScale = settFromCanvas.transform.localScale;
    }
    public void BackPanel () {
        MCamera.transform.position = StartPosMCamera;
        startCanvas = GameObject.Find ("StartAchievementsPos");
        if (startCanvas != null && setCanvas != null) {
            setCanvas.transform.position = startCanvas.transform.position;
            setCanvas.transform.rotation = startCanvas.transform.rotation;
            setCanvas.transform.localScale = startCanvas.transform.localScale;
        }
    }
}