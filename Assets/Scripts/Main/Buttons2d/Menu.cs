﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Menu : MonoBehaviour {

    public static Menu m_Instance { get; set; }
    GameObject MCamera;
    Vector3 StartPosMCamera;

    void Awake () {
        if (m_Instance == null) {
            m_Instance = this;
        } else if (m_Instance != null) {
            Destroy (gameObject);
        }
    }
    private void Start () {
        MCamera = GameObject.FindGameObjectWithTag ("MainCamera");
        StartPosMCamera = MCamera.transform.position;
    }
    private void Update () {
        if (Input.GetKeyDown (KeyCode.Escape)) {
            BackFromPanel ();
        }
    }
    public void OnMouseDown () {
        if (GM.gm_Instance.winCheker == false) {
            DisplayPanel ();
        }
    }

    public void DisplayPanel () {
        GameObject SCamera = GameObject.Find ("ToMenuCameraPos");
        MCamera.transform.position = SCamera.transform.position;
    }
    public void BackFromPanel () {
        MCamera.transform.position = StartPosMCamera;
    }

}