﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Question : MonoBehaviour {
    public static Question q_Instance { get; set; }
    GameObject startCanvas;
    Transform setToCanvas;
    GameObject settFromCanvas;
    GameObject MCamera;
    Vector3 StartPosMCamera;
    void Awake () {
        if (q_Instance == null) {
            q_Instance = this;
        } else if (q_Instance != null) {
            Destroy (gameObject);
        }
    }

    private void Start () {
        MCamera = GameObject.FindGameObjectWithTag ("MainCamera");
        StartPosMCamera = MCamera.transform.position;
    }
    private void Update () {
        if (Input.GetKeyDown (KeyCode.Escape)) {
            BackPanel ();
        }
    }
    public void OnMouseDown () {
        if (GM.gm_Instance.winCheker == false) {
            DisplayPanel ();
        }
    }

    public void DisplayPanel () {

        GameObject SCamera = GameObject.Find ("AbouttCameraPos");
        MCamera.transform.position = SCamera.transform.position;
        settFromCanvas = GameObject.Find ("AbouttCanvas");
        setToCanvas = AboutControl.ac_Instance.transform.parent;
        setToCanvas.GetChild (0).GetChild (0).gameObject.SetActive (true);
        setToCanvas.position = settFromCanvas.transform.position;
        setToCanvas.rotation = settFromCanvas.transform.rotation;
        setToCanvas.localScale = settFromCanvas.transform.localScale;
    }

    public void BackPanel () {
        MCamera.transform.position = StartPosMCamera;
        startCanvas = GameObject.Find ("StartAboutPos");
        if (startCanvas != null && setToCanvas != null) {
            setToCanvas.transform.position = startCanvas.transform.position;
            setToCanvas.transform.rotation = startCanvas.transform.rotation;
            setToCanvas.transform.localScale = startCanvas.transform.localScale;
        }
    }
}