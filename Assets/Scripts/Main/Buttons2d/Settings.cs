﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour, IPanelController {
    public static Settings s_Instance { get; set; }
    GameObject startCanvas;
    Transform setToCanvas;
    GameObject settFromCanvas;
    GameObject MCamera;
    Vector3 StartPosMCamera;
    void Awake () {
        if (s_Instance == null) {
            s_Instance = this;
        } else if (s_Instance != null) {
            Destroy (gameObject);
        }
    }

    private void Start () {
        MCamera = GameObject.FindGameObjectWithTag ("MainCamera");
        StartPosMCamera = MCamera.transform.position;
    }
    public void OnMouseDown () {
        if (GM.gm_Instance.winCheker == false) {
            DisplayPanel ();
        }
    }

    private void Update () {
        if (Input.GetKeyDown (KeyCode.Escape)) {
            BackFromPanel ();
        }
    }

    public void DisplayPanel () {
        GameObject SCamera = GameObject.Find ("SettCameraPos");
        MCamera.transform.position = SCamera.transform.position;
        settFromCanvas = GameObject.Find ("SettCanvas");
        setToCanvas = GameObject.Find ("SettingsHub").transform.GetChild(0).transform;
        setToCanvas.GetChild (0).gameObject.SetActive (true);
        setToCanvas.position = settFromCanvas.transform.position;
        setToCanvas.rotation = settFromCanvas.transform.rotation;
        setToCanvas.localScale = settFromCanvas.transform.localScale;
    }

    public void BackFromPanel () {
       
        MCamera.transform.position = StartPosMCamera;
        startCanvas = GameObject.Find ("StartSettingsPos");
        if (startCanvas != null && setToCanvas) {
            setToCanvas.transform.position = startCanvas.transform.position;
            setToCanvas.transform.rotation = startCanvas.transform.rotation;
            setToCanvas.transform.localScale = startCanvas.transform.localScale;
        }
    }
}