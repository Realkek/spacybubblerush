﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DataController : MonoBehaviour {
    public static DataController dC_Instance { get; set; }
    void Awake () {
        if (dC_Instance == null) {
            dC_Instance = this;
        } else if (dC_Instance != null) {
            Destroy (gameObject);
        }
    }
    void Start () {
        TimerToggle.tt_Instance.onChanged += TimerShowHide;
        LevelManager.lm_Instance.choosenLevel = SceneManager.GetActiveScene ().name;
    }
    public void TimerShowHide () {
        string tT = PlayerPrefs.GetString ("timerToggle");
        GameObject Timer = GameObject.FindWithTag ("Timer");
        switch (tT) {
            case "On":
                Timer.transform.GetChild (0).gameObject.SetActive (true);
                break;
            case "Off":
                Timer.transform.GetChild (0).gameObject.SetActive (false);
                break;
        }
    }

}