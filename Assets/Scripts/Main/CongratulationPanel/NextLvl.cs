﻿using System.Collections;
using System.Collections.Generic;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NextLvl : MonoBehaviour, IInterstitialAdListener {
    string currentLevel;
    // Start is called before the first frame update
    void Start () {
        currentLevel = LevelManager.lm_Instance.choosenLevel;
    }
    public void GoNext () {
        gameObject.GetComponent<Button> ().interactable = false;
        Appodeal.setInterstitialCallbacks (this);
        StartCoroutine (GetRequest ("https://www.google.com"));
    }
    private void ShowInterstitial () {
        if (Appodeal.isLoaded (Appodeal.INTERSTITIAL)) //Если реклама загружена, то показывается
            Appodeal.show (Appodeal.INTERSTITIAL);
    }

    IEnumerator GetRequest (string uri) {
        using (UnityWebRequest webRequest = UnityWebRequest.Get (uri)) {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest ();
            string[] pages = uri.Split ('/');
            int page = pages.Length - 1;
            if (webRequest.isNetworkError) {
                ShowLevel ();
            } else {
                ShowInterstitial ();
            }
            gameObject.GetComponent<Button> ().interactable = true;
        }
    }

    public void ShowLevel () {
        switch (currentLevel) {
            case "GamePlay_Level1":
                LevelManager.lm_Instance.choosenLevel = "GamePlay_Level2";
                SceneManager.LoadScene ("GamePlay_Level2");
                break;
            case "GamePlay_Level2":
                LevelManager.lm_Instance.choosenLevel = "GamePlay_Level3";
                SceneManager.LoadScene ("GamePlay_Level3");
                break;
            case "GamePlay_Level3":
                LevelManager.lm_Instance.choosenLevel = "GamePlay_Level4";
                SceneManager.LoadScene ("GamePlay_Level4");
                break;
            case "GamePlay_Level4":
                LevelManager.lm_Instance.choosenLevel = "GamePlay_Level5";
                SceneManager.LoadScene ("GamePlay_Level5");
                break;
            case "GamePlay_Level5":
                LevelManager.lm_Instance.choosenLevel = "GamePlay_Level6";
                SceneManager.LoadScene ("GamePlay_Level6");

                break;
        }
    }
    public void onInterstitialLoaded (bool isPrecache) {

    }

    public void onInterstitialFailedToLoad () {

    }

    public void onInterstitialShown () {
        throw new System.NotImplementedException ();
    }

    public void onInterstitialClosed () {
        ShowLevel ();
    }

    public void onInterstitialClicked () {
        throw new System.NotImplementedException ();
    }

    public void onInterstitialExpired () {
        ShowLevel ();
    }
}