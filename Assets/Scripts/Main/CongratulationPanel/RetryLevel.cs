﻿using System.Collections;
using System.Collections.Generic;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class RetryLevel : MonoBehaviour, IInterstitialAdListener {

    public void RetryLvl () {
        gameObject.GetComponent<Button> ().interactable = false;
        Appodeal.setInterstitialCallbacks (this);
        StartCoroutine (GetRequest ("https://www.google.com"));
    }
    private void ShowInterstitial () {
        if (Appodeal.isLoaded (Appodeal.INTERSTITIAL)) //Если реклама загружена, то показывается
            Appodeal.show (Appodeal.INTERSTITIAL);
    }
    IEnumerator GetRequest (string uri) {
        using (UnityWebRequest webRequest = UnityWebRequest.Get (uri)) {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest ();
            string[] pages = uri.Split ('/');
            int page = pages.Length - 1;
            if (webRequest.isNetworkError) {
                ShowLevel ();
            } else {
                ShowInterstitial ();
            }
            gameObject.GetComponent<Button> ().interactable = true;
        }
    }
    void ShowLevel () {
        SceneManager.LoadScene (LevelManager.lm_Instance.choosenLevel);
    }
    public void onInterstitialLoaded (bool isPrecache) {

    }
    public void onInterstitialFailedToLoad () { }

    public void onInterstitialShown () {
        throw new System.NotImplementedException ();
    }

    public void onInterstitialClosed () {
        ShowLevel ();
    }

    public void onInterstitialClicked () {
        throw new System.NotImplementedException ();
    }

    public void onInterstitialExpired () {
        ShowLevel ();
    }
}