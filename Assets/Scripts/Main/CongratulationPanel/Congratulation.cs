﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Congratulation : MonoBehaviour {
    public static Congratulation c_Instance { get; set; }
    // Start is called before the first frame update
    Scene scene;
    string scName;
    void Start () {
        if (c_Instance == null) {
            c_Instance = this;
        } else if (c_Instance != null) {
            Destroy (gameObject);
        }
        scene = SceneManager.GetActiveScene ();
        scName = scene.name;
    }

    public void ShowPanel () {
        Win.w_Instance.GoSound ();
        GM.gm_Instance.gmStatusCheker = false;
        SaveProgress ();
        transform.GetChild (0).gameObject.SetActive (true);
        GameObject PrevLvl = GameObject.Find("PrevLevel");
        if (scName == "GamePlay_Level1"){
            PrevLvl.SetActive(false);
        }
        else {
            PrevLvl.SetActive(true);
        }
    }

    void SaveProgress () {
        switch (scName) {
            case "GamePlay_Level1":
                Save.SaveLevel ("GamePlay_Level2", 1);
                break;
            case "GamePlay_Level2":
                Save.SaveLevel ("GamePlay_Level3", 1);
                break;
            case "GamePlay_Level3":
                Save.SaveLevel ("GamePlay_Level4", 1);
                break;
            case "GamePlay_Level4":
                Save.SaveLevel ("GamePlay_Level5", 1);
                break;
            case "GamePlay_Level5":
                Save.SaveLevel ("GamePlay_Level6", 1);
                break;
        }
    }
}