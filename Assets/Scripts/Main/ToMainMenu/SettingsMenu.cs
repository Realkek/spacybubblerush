﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsMenu : MonoBehaviour, IPanelController {
    public static SettingsMenu sm_Instance { get; set; }
    GameObject startCanvas;
    Transform setToCanvas;
    GameObject settFromCanvas;
    GameObject MCamera;
    Vector3 StartPosMCamera;
    void Awake () {
        if (sm_Instance == null) {
            sm_Instance = this;
        } else if (sm_Instance != null) {
            Destroy (gameObject);
        }
    }

    private void Start () {
        MCamera = GameObject.FindGameObjectWithTag ("MainCamera");
        StartPosMCamera = MCamera.transform.position;
    }
    public void OnMouseDown () {
        DisplayPanel ();
    }

    private void Update () {
        if (Input.GetKeyDown (KeyCode.Escape)) {
            BackFromPanel ();
        }
    }

    public void DisplayPanel () {

        GameObject SCamera = GameObject.Find ("SettCameraPos");
        MCamera.transform.position = SCamera.transform.position;
        settFromCanvas = GameObject.Find ("SettCanvas");
        setToCanvas = SettingsControl.sc_Instance.transform.parent;
        setToCanvas.GetChild (0).GetChild (0).gameObject.SetActive (true);
        setToCanvas.position = settFromCanvas.transform.position;
        setToCanvas.rotation = settFromCanvas.transform.rotation;
        setToCanvas.localScale = settFromCanvas.transform.localScale;
    }

    public void BackFromPanel () {
        MCamera.transform.position = StartPosMCamera;
        startCanvas = GameObject.Find ("StartSettingsPos");
        if (startCanvas != null && setToCanvas != null) {
            setToCanvas.transform.position = startCanvas.transform.position;
            setToCanvas.transform.rotation = startCanvas.transform.rotation;
            setToCanvas.transform.localScale = startCanvas.transform.localScale;
        }
    }
}