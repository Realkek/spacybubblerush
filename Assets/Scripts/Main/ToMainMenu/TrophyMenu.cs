﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrophyMenu : MonoBehaviour {
    public static TrophyMenu tm_Instance { get; set; }
    GameObject startCanvas;
    Transform setToCanvas;
    GameObject settFromCanvas;
    GameObject MCamera;
    Vector3 StartPosMCamera;
    void Awake () {
        if (tm_Instance == null) {
            tm_Instance = this;
        } else if (tm_Instance != null) {
            Destroy (gameObject);
        }
    }
    private void Start () {
        MCamera = GameObject.FindGameObjectWithTag ("MainCamera");
        StartPosMCamera = MCamera.transform.position;
    }
    private void Update () {
        if (Input.GetKeyDown (KeyCode.Escape)) {
            BackFromPanel ();
        }
    }
    public void OnMouseDown () {
        DisplayPanel ();
    }

    private void DisplayPanel () {

        GameObject SCamera = GameObject.Find ("AchievementtCameraPos");
        MCamera.transform.position = SCamera.transform.position;
        settFromCanvas = GameObject.Find ("AchievementtCanvas");
        setToCanvas = AchievementControl.ac_Instance.transform.parent;
        setToCanvas.GetChild (0).GetChild (0).gameObject.SetActive (true);
        setToCanvas.position = settFromCanvas.transform.position;
        setToCanvas.rotation = settFromCanvas.transform.rotation;
        setToCanvas.localScale = settFromCanvas.transform.localScale;
    }
    private void BackFromPanel () {
        MCamera.transform.position = StartPosMCamera;

        startCanvas = GameObject.Find ("StartAchievementsPos");
        if (startCanvas != null && setToCanvas!=null) {
            setToCanvas.transform.position = startCanvas.transform.position;
            setToCanvas.transform.rotation = startCanvas.transform.rotation;
            setToCanvas.transform.localScale = startCanvas.transform.localScale;
        }

    }
}