﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
    public float milisecond;
    public int second;
    public int minute;
    public Text text;
    private bool isAvalible;

    public static Timer t_Instance { get; set; }
    void Awake () {
        if (t_Instance == null) {
            t_Instance = this;
        } else if (t_Instance != null) {
            Destroy (gameObject);
        }
    }
    void Start () {
        DataController.dC_Instance.TimerShowHide ();
    }

    void FixedUpdate () {
        calculate ();
    }

    void calculate () {

        if (GM.gm_Instance.gmStatusCheker == true) {
            milisecond += 0.02f;
            if (milisecond >= 1f) {
                second++;
                milisecond = 0;
            }
            if (second == 60) {
                minute++;
                second = 0;
            }
            text.text = $"{minute} : {second}";
        }

    }

    public void UpdateScore () {
        AchieveController.ac_Instance.CheckScore (minute, second);
    }

}