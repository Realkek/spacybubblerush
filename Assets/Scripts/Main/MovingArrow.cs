﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingArrow : MonoBehaviour {
    public static MovingArrow ma_Instance { get; set; }
    // Start is called before the first frame update
    void Awake () {
        if (ma_Instance == null) {
            ma_Instance = this;
        } else if (ma_Instance != null) {
            Destroy (gameObject);
        }
    }

    // Update is called once per frame
    void Update () {

    }

    public void Moving(){
        transform.Translate (3.14f, Time.deltaTime, 0, Space.World); 
        OnArrowMove.oam_Instance.GoSound();
    }
}