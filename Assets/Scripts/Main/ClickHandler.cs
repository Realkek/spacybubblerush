﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickHandler : MonoBehaviour {
    public static ClickHandler ch_Instance { get; set; }

    List<int> dividerCount = new List<int> (); // счетчик количества возможных делимых для каждого из делителей (comparer)
    // Start is called before the first frame update
    void Awake () {
        ch_Instance = this;
    }
    // Update is called once per frame
    void Update () { }

    void OnMouseDown () {
        dividerCount = RandDividend.rd_Instance.forCompR;
        CheckMoves ();
    }

    public void CheckMoves () {

        TextMesh textObject;
        Renderer rend;

        textObject = transform.GetChild (0).GetComponent<TextMesh> ();

        rend = transform.GetComponent<Renderer> ();
        Color rendColor = rend.material.color;
        if (rendColor != Color.yellow&&rendColor != Color.magenta&&rendColor != Color.red&&rendColor != Color.green) {
            if (GM.gm_Instance.winCheker == false) {
                switch (RandDividend.rd_Instance.dividerNum) {
                    case 0:
                        if (RandDividend.rd_Instance.dividendCC < dividerCount[0] && Convert.ToInt32 (textObject.text) % RandDivider.rd_Instance.dividersText[0] == 0) {
                            rend.material.color = Color.yellow;
                            RandDividend.rd_Instance.dividendCC++;
                            if (RandDividend.rd_Instance.dividendCC >= dividerCount[0]) {
                                if (RandDividend.rd_Instance.dividerNum < (dividerCount.Count - 1)) {
                                    RandDividend.rd_Instance.dividerNum++;
                                    RandDividend.rd_Instance.dividendCC = 0;
                                    MovingArrow.ma_Instance.Moving ();
                                    RandDividend.rd_Instance.ClearColors ();
                                }
                            }
                        } else {
                            CheckingHearts.ch_Instance.hpMinus ();
                        };
                        break;
                    case 1:
                        if (RandDividend.rd_Instance.dividendCC < dividerCount[1] && Convert.ToInt32 (textObject.text) % RandDivider.rd_Instance.dividersText[1] == 0) {
                            rend.material.color = Color.magenta;
                            RandDividend.rd_Instance.dividendCC++;
                            if (RandDividend.rd_Instance.dividendCC >= dividerCount[1]) {
                                if (RandDividend.rd_Instance.dividerNum < (dividerCount.Count - 1)) {
                                    RandDividend.rd_Instance.dividerNum++;
                                    RandDividend.rd_Instance.dividendCC = 0;
                                    MovingArrow.ma_Instance.Moving ();
                                    RandDividend.rd_Instance.ClearColors ();
                                } else if (GM.gm_Instance.winCheker == false) {
                                    GM.gm_Instance.winCheker = true;
                                    Timer.t_Instance.UpdateScore ();
                                    Congratulation.c_Instance.ShowPanel ();
                                }
                            }
                        } else {
                            CheckingHearts.ch_Instance.hpMinus ();
                        };
                        break;

                    case 2:
                        if (RandDividend.rd_Instance.dividendCC < dividerCount[2] && Convert.ToInt32 (textObject.text) % RandDivider.rd_Instance.dividersText[2] == 0) {
                            rend.material.color = Color.red;
                            RandDividend.rd_Instance.dividendCC++;
                            if (RandDividend.rd_Instance.dividendCC >= dividerCount[2]) {
                                if (RandDividend.rd_Instance.dividerNum < (dividerCount.Count - 1)) {
                                    RandDividend.rd_Instance.dividerNum++;
                                    RandDividend.rd_Instance.dividendCC = 0;
                                    MovingArrow.ma_Instance.Moving ();
                                    RandDividend.rd_Instance.ClearColors ();
                                } else {
                                    GM.gm_Instance.winCheker = true;
                                    Timer.t_Instance.UpdateScore ();
                                    Congratulation.c_Instance.ShowPanel ();
                                }
                            }
                        } else {
                            CheckingHearts.ch_Instance.hpMinus ();
                        };
                        break;
                    case 3:
                        if (RandDividend.rd_Instance.dividendCC < dividerCount[3] && Convert.ToInt32 (textObject.text) % RandDivider.rd_Instance.dividersText[3] == 0) {
                            rend.material.color = Color.green;
                            RandDividend.rd_Instance.dividendCC++;
                            if (RandDividend.rd_Instance.dividendCC >= dividerCount[3]) {
                                GM.gm_Instance.winCheker = true;
                                Timer.t_Instance.UpdateScore ();
                                Congratulation.c_Instance.ShowPanel ();
                            }
                        } else {
                            CheckingHearts.ch_Instance.hpMinus ();
                        }
                        break;
                }
            }
        }
    }

}