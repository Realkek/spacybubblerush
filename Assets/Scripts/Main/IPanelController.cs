﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPanelController {
    void DisplayPanel ();
    void BackFromPanel();
}