﻿using System.Collections;
using System.Collections.Generic;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine;
public class AdsManager : MonoBehaviour {
    public static AdsManager am_Instance { get; set; }
    private const string APP_KEY = "96bf3beee5b0a391501545e04c357077ba089b037cd1bf36";
    private readonly string[] DISABLED_NETWORKS = { "facebook", "flurry", "pubnative", "inmobi" };

    void Awake () {
        if (am_Instance == null) {
            am_Instance = this;
        } else if (am_Instance != null) {
            Destroy (gameObject);
        }
    }

    private void Start () {
        Initialize (true);
    }

    private void Initialize (bool isTesting) {
        Appodeal.setTesting (isTesting);
        Appodeal.muteVideosIfCallsMuted (true);
        foreach (string nw in DISABLED_NETWORKS) {
            Appodeal.disableNetwork (nw);
        }
        // Appodeal.initialize(APP_KEY, Appodeal.INTERSTITIAL | Appodeal.NON_SKIPPABLE_VIDEO);
        Appodeal.initialize (APP_KEY, Appodeal.INTERSTITIAL);
    }

    public void ShowInterstitial () {
        if (Appodeal.isLoaded (Appodeal.INTERSTITIAL)) //Если реклама загружена, то показывается
            Appodeal.show (Appodeal.INTERSTITIAL);
    }

    public IEnumerator IEShowNonSkippable () {
        yield return new WaitUntil (() => Appodeal.canShow (Appodeal.NON_SKIPPABLE_VIDEO));
        Appodeal.show (Appodeal.NON_SKIPPABLE_VIDEO);
    }

    public void ShowNonSkippable () {
        if (Appodeal.canShow (Appodeal.NON_SKIPPABLE_VIDEO))
            Appodeal.show (Appodeal.NON_SKIPPABLE_VIDEO);
    }
}