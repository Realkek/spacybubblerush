﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quit : MonoBehaviour {
    public static Quit q_Instance { get; set; }
    void Awake () {
        if (q_Instance == null) {
            q_Instance = this;
        } else if (q_Instance != null) {
            Destroy (gameObject);
        }
    }

    void Update () {
        if (Input.GetKeyDown (KeyCode.Escape)) {
            HideSettings.q_Instance.BackFromPanel ();
        }
    }
}