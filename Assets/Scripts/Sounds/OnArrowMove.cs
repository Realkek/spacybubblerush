﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnArrowMove : MonoBehaviour {
    public static OnArrowMove oam_Instance { get; set; }
    public AudioClip auClip;

    void Awake () {
        if (oam_Instance == null) {
            oam_Instance = this;
        } else if (oam_Instance != null) {
            Destroy (gameObject);
        }
    }
    void Sound (AudioClip clip) {
        GetComponent<AudioSource> ().PlayOneShot (clip);
    }
    public void GoSound(){
        Sound(auClip);
    }
}