﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WrongChoice : MonoBehaviour {
    public static WrongChoice wc_Instance { get; set; }
    public AudioClip auClip;

    void Awake () {
        if (wc_Instance == null) {
            wc_Instance = this;
        } else if (wc_Instance != null) {
            Destroy (gameObject);
        }
    }
    void Sound (AudioClip clip) {
        GetComponent<AudioSource> ().PlayOneShot (clip);
    }
    public void GoSound () {
        Sound (auClip);
    }
}