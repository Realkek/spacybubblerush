﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour {
    public static GameOver go_Instance { get; set; }
    public AudioClip auClip;

    void Awake () {
        if (go_Instance == null) {
            go_Instance = this;
        } else if (go_Instance != null) {
            Destroy (gameObject);
        }
    }
    private void Start() {
        GoSound();
    }
    void Sound (AudioClip clip) {
        GetComponent<AudioSource> ().PlayOneShot (clip);
    }
    public void GoSound () {
        Sound (auClip);
    }
}