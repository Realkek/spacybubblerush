﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Win : MonoBehaviour
{
   public static Win w_Instance { get; set; }
    public AudioClip auClip;

    void Awake () {
        if (w_Instance == null) {
            w_Instance = this;
        } else if (w_Instance != null) {
            Destroy (gameObject);
        }
    }
    void Sound (AudioClip clip) {
        GetComponent<AudioSource> ().PlayOneShot (clip);
    }
    public void GoSound () {
        Sound (auClip);
    }
}
