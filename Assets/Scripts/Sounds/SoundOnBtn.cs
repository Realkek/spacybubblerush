﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOnBtn : MonoBehaviour {
  public AudioClip auClip;
  public void GoSound (AudioClip clip) {
    GetComponent<AudioSource> ().PlayOneShot (clip);
  }
  void OnMouseDown () {
    GoSound (auClip);
  }
}