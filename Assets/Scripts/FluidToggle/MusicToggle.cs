﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
public class MusicToggle : MonoBehaviour {
    public static MusicToggle mt_Instance { get; set; }
    TextMeshProUGUI textich;
    GameObject musa;
    void Awake () {
        if (mt_Instance == null) {
            mt_Instance = this;
        } 
        // else if (mt_Instance != null) {
        //     Destroy (gameObject);
        // }
    }

    public void TextChanged () {

        textich = transform.GetComponent<TextMeshProUGUI> ();
        musa = GameObject.Find ("Audio");
        if (textich.text == "On") {
            musa.transform.GetChild (0).gameObject.SetActive (true);
        } else if (textich.text == "Off") {
            musa.transform.GetChild (0).gameObject.SetActive (false);
        }
    }
}