﻿using DG.Tweening;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace LostNative.Toolkit.FluidUI {
    public class FluidToggle : MonoBehaviour {
        public delegate void ToggleDelegate (bool optionASelected);
        public ToggleDelegate OnToggle;
        float targetXPosition = 0;
        string nameParrent;
        [SerializeField] protected RectTransform toggleRectTransform;
        [SerializeField] protected Image toggleContainerImage;
        [SerializeField] protected Image toggleLeftImage;
        [SerializeField] protected Image toggleCenterImage;
        [SerializeField] protected Image toggleRightImage;

        [Header ("Toggle Colors")]

        [SerializeField] protected Color toggleContainerColor;
        [SerializeField] protected Color toggleColor;
        [SerializeField] protected Color optionALabelColor;
        [SerializeField] protected Color optionBLabelColor;

        [Header ("Toggle Label")]

        [SerializeField] protected TextMeshProUGUI toggleLabel;
        [SerializeField] protected string optionAText;
        [SerializeField] protected string optionBText;

        [Header ("Animation Variables")]

        [SerializeField] protected float stretchTime = 0.2f;
        [SerializeField] protected float movementTime = 0.25f;
        [SerializeField] protected float compressedTime = 0.2f;
        [SerializeField] protected float stretchedSize = 117f;
        [SerializeField] protected float compressedSize = 35f;

        private float targetXPositionA;
        private float targetXPositionB;

        private float pivotResetPositionA;
        private float pivotResetPositionB;

        private const float RightPivot = 0f;
        private const float LeftPivot = 1f;

        private Sequence toggleSequence;

        private bool toggleSequenceAlive;
        private bool optionASelected;

        private void Start () {
            Init (true);
        }

        public void Init (bool initialOptionA) {
            toggleSequenceAlive = false;
            nameParrent = transform.parent.name;
            optionASelected = initialOptionA;
            HandleSizing ();

            HandleColors ();

            StartToggle (optionASelected);
            firstOpen ();
            toggleSequence.Complete ();

        }

        private void HandleSizing () {
            pivotResetPositionA = toggleRectTransform.anchoredPosition.x;
            targetXPositionB = toggleRectTransform.anchoredPosition.x * -1f;

            toggleRectTransform.sizeDelta = new Vector2 (compressedSize, toggleRectTransform.sizeDelta.y);

            targetXPositionA = toggleRectTransform.anchoredPosition.x - toggleRectTransform.sizeDelta.x;

            pivotResetPositionB = targetXPositionB - toggleRectTransform.sizeDelta.x;
        }

        private void HandleColors () {
            toggleContainerImage.color = toggleContainerColor;
            toggleLeftImage.color = toggleColor;
            toggleCenterImage.color = toggleColor;
            toggleRightImage.color = toggleColor;
        }

        protected void StartToggle (bool targetOptionA) {

            switch (nameParrent) {
                case "Sounds":
                    FluidSoundToggle (targetOptionA);
                    break;
                case "Music":
                    FluidMusicToggle (targetOptionA);
                    break;
                case "Timer":
                    FluidTimerToggle (targetOptionA);
                    break;
            }
            UpdateToggleLabel ();
            StartToggleSequence (targetXPosition);
        }

        public void firstOpen () {
            //инициализация
            if (!PlayerPrefs.HasKey ("IsFirstRun")) {
                PlayerPrefs.SetInt ("IsFirstRun", 1);
            } else {
                PlayerPrefs.SetInt ("IsFirstRun", 0);
            }

            //если первый запуск
            if (PlayerPrefs.GetInt ("IsFirstRun") == 1) {
                optionASelected = true;

                toggleLabel.text = optionBText;
                toggleLabel.color = optionBLabelColor;

                SoundsToggle.st_Instance.TextChanged ();

                MusicToggle.mt_Instance.TextChanged ();

                TimerToggle.tt_Instance.TextChanged ();

                StartToggleSequence (targetXPosition);
            }
        }

        private void FluidMusicToggle (bool OptionA) {
            if (targetXPosition == 0 && PlayerPrefs.HasKey ("musicset")) {
                targetXPosition = PlayerPrefs.GetFloat ("musicset");
            } else {
                targetXPosition = OptionA ? targetXPositionA : targetXPositionB;
                PlayerPrefs.SetFloat ("musicset", targetXPosition);
            }
            if (targetXPosition >= 0) {
                optionASelected = false;
            } else {
                optionASelected = true;
            }
        }

        private void FluidSoundToggle (bool OptionA) {
            if (targetXPosition == 0 && PlayerPrefs.HasKey ("soundSet")) {
                targetXPosition = PlayerPrefs.GetFloat ("soundSet");
            } else {
                targetXPosition = OptionA ? targetXPositionA : targetXPositionB;
                PlayerPrefs.SetFloat ("soundSet", targetXPosition);
            }
            if (targetXPosition >= 0) {
                optionASelected = false;
            } else {
                optionASelected = true;
            }
        }
        private void FluidTimerToggle (bool OptionA) {
            if (targetXPosition == 0 && PlayerPrefs.HasKey ("timerSet")) {
                targetXPosition = PlayerPrefs.GetFloat ("timerSet");
            } else {
                targetXPosition = OptionA ? targetXPositionA : targetXPositionB;
                PlayerPrefs.SetFloat ("timerSet", targetXPosition);
            }
            if (targetXPosition >= 0) {
                optionASelected = false;
            } else {
                optionASelected = true;
            }
        }

        private void UpdateToggleLabel () {
            toggleLabel.text = optionASelected ? optionAText : optionBText;
            toggleLabel.color = optionASelected ? optionALabelColor : optionBLabelColor;
            switch (nameParrent) {
                case "Sounds":
                    SoundsToggle.st_Instance.TextChanged ();
                    break;
                case "Music":
                    MusicToggle.mt_Instance.TextChanged ();
                    break;
                case "Timer":
                    TimerToggle.tt_Instance.TextChanged ();
                    break;
            }
        }

        protected virtual void StartToggleSequence (float targetXPosition) {
            toggleSequence = DOTween.Sequence ();
            var positionTween = toggleRectTransform.DOAnchorPosX (targetXPosition, movementTime);
            var stretchTween = toggleRectTransform.DOSizeDelta (new Vector2 (stretchedSize, toggleRectTransform.sizeDelta.y), stretchTime);
            var compressTween = toggleRectTransform.DOSizeDelta (new Vector2 (compressedSize, toggleRectTransform.sizeDelta.y), compressedTime);
            toggleSequence.Insert (0f, positionTween);
            toggleSequence.Insert (0f, stretchTween);
            toggleSequence.Insert (stretchTime / 2, compressTween);
            toggleSequence.OnComplete (OnToggleSequenceComplete);
            toggleSequence.Play ();
            toggleSequenceAlive = true;
        }

        private void OnToggleSequenceComplete () {
            ResetPositionAtPivot ();
            optionASelected = !optionASelected;
            toggleSequenceAlive = false;
        }

        private void ResetPositionAtPivot () {
            if (optionASelected) {
                toggleRectTransform.pivot = new Vector2 (LeftPivot, toggleRectTransform.pivot.y);
                toggleRectTransform.anchoredPosition = new Vector2 (pivotResetPositionA, toggleRectTransform.anchoredPosition.y);
            } else {
                toggleRectTransform.pivot = new Vector2 (RightPivot, toggleRectTransform.pivot.y);
                toggleRectTransform.anchoredPosition = new Vector2 (pivotResetPositionB, toggleRectTransform.anchoredPosition.y);
            }
        }

        [UsedImplicitly]
        public virtual void OnToggleClick () {
            FinishCurrentToggle ();

            StartToggle (optionASelected);

            OnToggle?.Invoke (optionASelected);
        }

        protected void FinishCurrentToggle () {
            if (toggleSequenceAlive)
                toggleSequence.Complete ();
        }
    }
}