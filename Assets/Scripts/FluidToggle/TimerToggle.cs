﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
public class TimerToggle : MonoBehaviour {
    TextMeshProUGUI textich;
    public static TimerToggle tt_Instance { get; set; }
    public delegate void MethodContainer ();
    public event MethodContainer onChanged;
    void Awake () {
        if (tt_Instance == null) {
            tt_Instance = this;
        } else if (tt_Instance != null) {
            Destroy (gameObject);
        }
    }

    public void TextChanged () {
        textich = transform.GetComponent<TextMeshProUGUI> ();
        if (textich.text == "On") {
            PlayerPrefs.SetString ("timerToggle", "On");
        } else if (textich.text == "Off") {
            PlayerPrefs.SetString ("timerToggle", "Off");
        }
        if (DataController.dC_Instance != null)
            onChanged ();

    }
}