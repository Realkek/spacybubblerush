﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SoundsToggle : MonoBehaviour {
    TextMeshProUGUI textich;
    public static SoundsToggle st_Instance { get; set; }
    public delegate void onChangeContainer ();
    public event onChangeContainer onChangedToggle;

    private void Awake () {
        if (st_Instance == null) {
            st_Instance = this;
        } else if (st_Instance != null) {
            Destroy (gameObject);
        }
        GoPlay.gp_Instance.ActivateSToggle ();
    }

   
    public void TextChanged () {
        textich = transform.GetComponent<TextMeshProUGUI> ();
        if (textich.text == "On") {
            PlayerPrefs.SetString ("soundsToggle", "On");
        } else if (textich.text == "Off") {
            PlayerPrefs.SetString ("soundsToggle", "Off");
        }
        try {
            onChangedToggle ();
        } catch (System.NullReferenceException) {

        }
    }
}